﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class DashboardPage : AbstractInternalPage
    {
        private OrganizationsPage FromOrganizationsPage;

        private By Widgets = By.CssSelector("div[class=\"dashboard-widget-content\"]");

        public DashboardPage(IWebDriver driver, OrganizationsPage organizationPage) : base(driver)
        {
            FromOrganizationsPage = organizationPage;
        }

        public DashboardPage(IWebDriver driver, AbstractInternalPage prevPage) : base(driver, prevPage)
        {
        }

        protected override bool EvaluateLoadedStatus()
        {
            var elementArray = Elements(Widgets);
            if (elementArray.Length == 0)
                return false;
            foreach (var element in elementArray)
            {
                if (!element.Displayed)
                    return false;
            }
            return true;
        }

        protected override void ExecuteLoad()
        {
            if (PreviousPage == null)
            {
                FromOrganizationsPage.Load();
                FromOrganizationsPage.SelectOrganization();
            }
            else
            {
                PreviousPage.Load();
                PreviousPage.Menu.GoToDashboard();
            }
            VisibleElements(Widgets, TimeSpan.FromSeconds(5));
        }
    }
}

