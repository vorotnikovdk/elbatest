﻿using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public abstract class AbstractInternalPage : AbstractComponent<AbstractInternalPage>
    {
        protected AbstractInternalPage PreviousPage;

        public AbstractInternalPage(IWebDriver driver) : base(driver)
        {
        }

        public AbstractInternalPage(IWebDriver driver, AbstractInternalPage prevPage) : base(driver)
        {
            PreviousPage = prevPage;
        }

        public MainMenu Menu {
            get
            {
                return new MainMenu(Driver, this).Load();
            }
        }
    }
}

