﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class MainMenu : AbstractComponent<MainMenu>
	{
        private AbstractInternalPage ParentPage;

        private By Menu = By.Id("SidebarPanel");
        private By MenuToggle = By.Id("SidebarToggle");
        private By DashboardLink = By.Id("MainMenu_Dashboard_Link");
        private By PaymentsLink = By.Id("MainMenu_Payments_Link");

        public MainMenu(IWebDriver driver, AbstractInternalPage parent) : base(driver)
		{
            ParentPage = parent;
		}
        
		public PaymentsPage GoToPayments()
		{
			Element(PaymentsLink).Click();
			return new PaymentsPage(Driver, ParentPage);
		}

        public DashboardPage GoToDashboard()
        {
            Element(DashboardLink).Click();
            return new DashboardPage(Driver, ParentPage);
        }

        protected override void ExecuteLoad()
        {
            Element(MenuToggle).Click();
            VisibleElement(Menu, TimeSpan.FromSeconds(1));
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(Menu).Displayed;
        }
    }
}

