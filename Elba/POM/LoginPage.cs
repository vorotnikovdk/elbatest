﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class LoginPage : AbstractComponent<LoginPage>
    {
        private string UserEmail;
        private string UserPassword;

        private By Email = By.Id("Email");
        private By Password = By.Id("Password");
        private By LoginButton = By.Id("LoginButton");

        public LoginPage(IWebDriver driver, string email, string password) : base(driver)
        {
            UserEmail = email;
            UserPassword = password;
        }

        public void LogIn(string email, string password)
        {
			Element(Email).SendKeys(email);
            Element(Password).SendKeys(password);
            Element(LoginButton).Click();;
        }

        public void LogIn()
        {
            LogIn(UserEmail, UserPassword);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(LoginButton).Enabled;
        }

        protected override void ExecuteLoad()
        {
            Driver.Url = "https://elba.kontur.ru/AccessControl/Login.aspx?returnurl=https%3A%2F%2Felba.kontur.ru%2FOutsourcer%2FSelectOrganization";
            VisibleElement(LoginButton, TimeSpan.FromSeconds(15));
        }
    }
}
