﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace ElbaTests.POM
{
    public abstract class AbstractComponent<T>: LoadableComponent<T>
        where T : AbstractComponent<T>
    {
        protected IWebDriver Driver;

        public AbstractComponent(IWebDriver driver)
        {
            Driver = driver;
        }

        protected IWebElement Element(By locator)
        {
            return Driver.FindElement(locator);
        }

        protected IWebElement[] Elements(By locator)
        {
            var collection = Driver.FindElements(locator);
            IWebElement[] elements = new IWebElement[collection.Count];
            collection.CopyTo(elements, 0);
            return elements;
        }

        protected IWebElement VisibleElement(By locator, TimeSpan timeout)
        {
            var wait = new WebDriverWait(Driver, timeout);
            return wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        protected IWebElement[] VisibleElements(By locator, TimeSpan timeout)
        {
            var wait = new WebDriverWait(Driver, timeout);
            var collection = wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
            IWebElement[] elements = new IWebElement[collection.Count];
            collection.CopyTo(elements, 0);
            return elements;
        }

        public string GetTitle()
        {
            return Driver.Title;
        }
    }
}