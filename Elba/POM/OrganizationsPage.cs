﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class OrganizationsPage : AbstractComponent<OrganizationsPage>
    {
        private int TargetOrganization;
        private LoginPage FromLoginPage;

        private By Organizations = By.CssSelector("a[id^=\"OrganizationTable_row\"][id*=\"_Name\"]");
        private By OrganizationsTable = By.Id("OrganizationTable");

        public OrganizationsPage(IWebDriver driver, LoginPage loginPage, int targetOrganization) : base(driver)
        {
            FromLoginPage = loginPage;
            TargetOrganization = targetOrganization;
        }

        public void SelectOrganization()
        {
            Elements(Organizations)[TargetOrganization].Click();
        }

        protected override void ExecuteLoad()
        {
            FromLoginPage.Load();
            FromLoginPage.LogIn();
            VisibleElement(OrganizationsTable, TimeSpan.FromSeconds(5));
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(OrganizationsTable).Displayed;
        }
    }
}

