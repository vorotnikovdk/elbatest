﻿using System;

namespace ElbaTests.POM
{
    public struct PaymentInfo
    {
        public string Contractor { get; private set; }
        public decimal Sum { get; private set; }
        public decimal TaxSum { get; private set; }
        public string Description { get; private set; }
        public DateTime Date { get; private set; }
        public int Number { get; private set; }

        public PaymentInfo(string contractor, decimal sum, decimal taxsum,
            string descripton, DateTime date, int number)
        {
            Contractor = contractor;
            Sum = sum;
            TaxSum = taxsum;
            Description = descripton;
            Date = date;
            Number = number;
        }
    }

}