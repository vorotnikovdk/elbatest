﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class PaymentRecord : AbstractComponent<PaymentRecord>
    {
        private By Record = By.CssSelector("div[class*=\"_imported\"]");
        private string CachedId;
        private string Id {
            get
            {
                if (CachedId == null)
                {
                    string fullId = VisibleElement(Record,
                        TimeSpan.FromSeconds(15)).GetAttribute("id");
                    CachedId = fullId.Substring(0, fullId.IndexOf("_") + 1);
                }
                return CachedId;
            }
        }
        private By ContractorName { get{ return By.Id(Id + "ContractorItem_ContractorName"); } }
        private By Sum { get{ return By.Id(Id + "Sum"); } }
        private By TaxSum { get { return By.Id(Id + "TaxSum"); } }
        private By Description { get { return By.Id(Id + "Description"); } }
        private By DateAsString { get { return By.Id(Id + "DateAsString"); } }
        private By DocumentNumber { get { return By.Id(Id + "DocumentNumber"); } }

        public PaymentRecord(IWebDriver driver) : base(driver)
        {
        }

        public string GetContractor()
        {
            return Element(ContractorName).Text;
        }

        public PaymentInfo GetInfo()
        {
            string contractor = GetContractor();
            decimal sum = Convert.ToDecimal(Element(Sum).Text);
            string tax = Element(TaxSum).Text;
            decimal taxsum = tax.Length == 0 ? 0 : Convert.ToDecimal(tax);
            string description = Element(Description).Text;
            DateTime date = Convert.ToDateTime(Element(DateAsString).Text);
            int number = Convert.ToInt32(Element(DocumentNumber).Text.Substring(1));
            return new PaymentInfo(contractor, sum, taxsum, description, date, number);
        }

        public PaymentRecord Edit(PaymentInfo info)
        {
            Element(Record).Click();
            var editBox = new PaymentRecordEdit(Driver).Load();
            editBox.EditPayment(info);
            return new PaymentRecord(Driver);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(Record).Displayed;
        }

        protected override void ExecuteLoad()
        {
            VisibleElement(Record, TimeSpan.FromSeconds(10));  
        }
    }
}
