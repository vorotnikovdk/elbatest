﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ElbaTests.POM
{
    public class PaymentsPage : AbstractInternalPage
    {
        private By UploadFileButton = By.Id("PaymentUpload_PaymentFileUpload_Input");
        private By NoResults = By.Id("NoSourceItemsPanel");
        private By Results = By.ClassName("paymentsList-item");
        private By PaymentsTableFooter = By.Id("Footer_IncomeExpenseList");
        private By SelectAll = By.Id("ItemsList_CheckAllRows_caption");
        private By DeleteLink = By.Id("ComponentsHost_PaymentsMultipleActionsDialog_DeletePseudolink");
        private By DeleteConfirm = By.Id("ComponentsHost_DeleteConfirmationDialog_DeleteButton");

        public PaymentsPage(IWebDriver driver, AbstractInternalPage prevPage) : base(driver)
        {
            PreviousPage = prevPage;
        }

        public PaymentRecord UploadValidPayment(string path)
        {
            int numOfResults = Elements(Results).Length;
            Element(UploadFileButton).SendKeys(path);
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(d => {
                try
                {
                    return numOfResults < d.FindElements(Results).Count;
                }
                catch (NullReferenceException)
                {
                    // IE sometimes throws NRE in FindElements()
                    // Just try again
                    Console.WriteLine("NULL");
                    return false;
                }
                 });
            return new PaymentRecord(Driver);
        }

        public PaymentError UploadInvalidPayment(string path)
        {
            Element(UploadFileButton).SendKeys(path);
            return new PaymentError(Driver);
        }

        public void DeleteRecords()
        {
            if (!Element(NoResults).Displayed)
            {
                Element(SelectAll).Click();
                VisibleElement(DeleteLink, TimeSpan.FromSeconds(2)).Click();
                VisibleElement(DeleteConfirm, TimeSpan.FromSeconds(2)).Click();
            }
            VisibleElement(NoResults, TimeSpan.FromSeconds(5));
        }
        
        protected override void ExecuteLoad()
        {
            PreviousPage.Load();
            PreviousPage.Menu.GoToPayments();
            try
            {
                VisibleElement(NoResults, TimeSpan.FromSeconds(5));
            }
            catch (WebDriverTimeoutException)
            {
                VisibleElement(PaymentsTableFooter, TimeSpan.FromSeconds(20));
            }
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(NoResults).Displayed || Element(PaymentsTableFooter).Displayed;
        }
    }
}

