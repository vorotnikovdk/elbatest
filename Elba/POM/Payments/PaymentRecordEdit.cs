﻿using System;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    class PaymentRecordEdit : AbstractComponent<PaymentRecordEdit>
    {
        const string Prefix = "ComponentsHost_PaymentEditLightbox_";
        private By AcceptButton = By.Id(Prefix + "AcceptButton");
        private By Contractor = By.Id(Prefix + "Contractor_ContractorName");
        private By Sum = By.Id(Prefix + "IncomeSum");
        private By TaxSum = By.Id(Prefix + "TaxSum");
        private By Description = By.Id(Prefix + "PaymentDescription");
        private By Date = By.Id(Prefix + "Date");
        private By Number = By.Id(Prefix + "DocumentNumber");

        public PaymentRecordEdit(IWebDriver driver) : base(driver)
        {
        }

        public void EditPayment(PaymentInfo info)
        {
            Element(Contractor).Clear();
            Element(Contractor).SendKeys(info.Contractor);
            Element(Sum).Clear();
            Element(Sum).SendKeys(info.Sum.ToString());
            Element(TaxSum).Clear();
            Element(TaxSum).SendKeys(info.TaxSum.ToString());
            Element(Description).Clear();
            Element(Description).SendKeys(info.Description);
            Element(Date).Clear();
            Element(Date).SendKeys(info.Date.ToShortDateString());
            Element(Number).Clear();
            Element(Number).SendKeys(info.Number.ToString());
            Element(AcceptButton).Click();
        }

        protected override bool EvaluateLoadedStatus()
        {
            var button = Element(AcceptButton);
            return button.Displayed && button.Enabled;
        }

        protected override void ExecuteLoad()
        {
            VisibleElement(AcceptButton, TimeSpan.FromSeconds(10));
        }
    }
}
