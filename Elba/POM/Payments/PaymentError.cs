﻿using System;
using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace ElbaTests.POM
{
    public class PaymentError : AbstractComponent<PaymentError>
    {
        private By ErrorDiv = By.Id("ImportFailuresMessage");
        private By ReasonLink = By.Id("ImportFailuresMessage_ReasonsLink");
        private By ReasonText = By.Id("ComponentsHost_FailuresLightbox_FailuresList_Rows");
        private By CloseReasonButton = By.Id("ComponentsHost_FailuresLightbox_AcceptButton");

        public PaymentError(IWebDriver driver) : base(driver)
        {
        }

        public int GetErrorCode()
        {
            Element(ReasonLink).Click();
            string reason = VisibleElement(ReasonText, TimeSpan.FromSeconds(5)).Text;
            Element(CloseReasonButton).Click();
            Match match = Regex.Match(reason, @"№ (\d+)");
            return Convert.ToInt32(match.Groups[1].Value);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Element(ErrorDiv).Displayed;
        }

        protected override void ExecuteLoad()
        {
            VisibleElement(ErrorDiv, TimeSpan.FromSeconds(10));
        }
    }

}