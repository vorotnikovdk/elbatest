﻿using ElbaTests.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

using System;

namespace ElbaTests
{
    [TestFixtureSource("Browsers")]
    public class TestPayments
    {
        const string FilesDir = @"d:\payments\";
        private string Browser;
        private IWebDriver Driver;
        private PaymentsPage Payments;

        static object[] Browsers = {
            new object[] { "Chrome" },
            new object[] { "PhantomJS" },
            new object[] { "InternetExplorer" },
        };

        public TestPayments(string browser)
        {
            Browser = browser;
        }

        [OneTimeSetUp]
        public void Init()
        {
            switch (Browser)
            {
                case "Chrome":
                    Driver = new ChromeDriver();
                    break;
                case "PhantomJS":
                    Driver = new PhantomJSDriver();
                    break;
                case "InternetExplorer":
                    Driver = new InternetExplorerDriver();
                    break;
            }
            Driver.Manage().Window.Maximize();

            var loginPage = new LoginPage(Driver, "kontur@testers.ru", "111111");
            var organizationsPage = new OrganizationsPage(Driver, loginPage, 483);
            var dashboard = new DashboardPage(Driver, organizationsPage);
            Payments = new PaymentsPage(Driver, dashboard);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            Payments.Load();
            Payments.DeleteRecords();
            Driver.Quit();
            Driver.Dispose();
        }

        [TestCase("FindByINN.txt", ExpectedResult = "ООО \"Рабочая лошадка\"")]
        [TestCase("FindByName.txt", ExpectedResult = "ПАО \"Роги и Копыты Анлимитед\"")]
        [TestCase("FindByBankAccount.txt", ExpectedResult = "ИП Фунт")]
        public string FindingContractorIsCorrect(string file)
        {
            Payments.Load();
            var Uploaded = Payments.UploadValidPayment(FilesDir + file).Load();
            return Uploaded.GetContractor();
        }

        static object[] ImportsToDisplay = {
            new object[] { "ImportIncoming.txt", new PaymentInfo(
                "ООО \"Омский ГАЗМЯС\"", 4500, 0,
                "Платеж ООО ГАЗМЯС", DateTime.Parse("09.02.2014"), 315
                ) },
            new object[] { "ImportIncomingNoDate.txt", new PaymentInfo(
                "ООО \"Омский ГАЗМЯС\"", 4500, 0,
                "Платеж ООО ГАЗМЯС", DateTime.Parse("10.02.2014"), 316
                ) },
            new object[] { "ImportOutcoming.txt", new PaymentInfo(
                "ООО \"Омский ГАЗМЯС\"", Convert.ToDecimal(666.66), 0,
                "Оплата для ООО ГАЗМЯС", DateTime.Parse("09.03.2014"), 317
                ) },
            new object[] { "ImportOutcomingNoDate.txt", new PaymentInfo(
                "ООО \"Омский ГАЗМЯС\"", Convert.ToDecimal(666.66), 0,
                "Оплата для ООО ГАЗМЯС", DateTime.Parse("10.03.2014"), 318
                ) },
            new object[] { "ImportDescription.txt", new PaymentInfo(
                 "ООО \"Омский ГАЗМЯС\"", 4500, 0,
                "Платеж для ООО ГАЗМЯС", DateTime.Parse("09.02.2014"), 319
                ) },
        };

        [TestCaseSource("ImportsToDisplay")]
        public void AllPaymentFieldsAreDisplayed(string file, PaymentInfo Info)
        {
            Payments.Load();
            var Uploaded = Payments.UploadValidPayment(FilesDir + file).Load();
            Assert.That(Uploaded.GetInfo(), Is.EqualTo(Info));
        }

        [TestCase("ErrorDate.txt", ExpectedResult = 320)]
        public int WrongImportEndsWithError(string file)
        {
            Payments.Load();
            var Error = Payments.UploadInvalidPayment(FilesDir + file).Load();
            return Error.GetErrorCode();
        }

        static object[] ImportsToEdit = {
            new object[] { "ImportsToEdit.txt", new PaymentInfo(
                "ООО \"Омский ГАЗМЯС\"", 1000, 999,
                "Платеж ООО ГАЗМЯС", DateTime.Parse("09.01.2017"), 399
                ) },
        };

        [TestCaseSource("ImportsToEdit")]
        public void AllEditedPaymentFieldsAreDisplayed(string file, PaymentInfo Info)
        {
            Payments.Load();
            var Uploaded = Payments.UploadValidPayment(FilesDir + file).Load();
            var Edited = Uploaded.Edit(Info).Load();
            Assert.That(Edited.GetInfo(), Is.EqualTo(Info));
        }
    }
}

